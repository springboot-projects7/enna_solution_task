package com.chandan.ennea.config;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.chandan.ennea.exception.ErrorCodes;
import com.chandan.ennea.exception.ErrorDetails;
import com.chandan.ennea.exception.ProductNotFoundException;


@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
	
private static final Logger LOG = LoggerFactory.getLogger(RestExceptionHandler.class);
	
	private static final String SQL_EXCEPTION_STR = "SQLServerException: ";
	private static final String EMPTY_STR = "";
	
	
	@ExceptionHandler(ProductNotFoundException.class)
	protected ResponseEntity<ErrorDetails> handleException(ProductNotFoundException baseException, WebRequest request) {
		LOG.debug("Starting handleException");
		ErrorDetails errorResponse = null;

		LOG.error("****Product Details Exception: ", baseException);

		if (null != baseException) {
			errorResponse = baseException.getErrorResponse();
			if(null!=errorResponse.getErrorDesc() && errorResponse.getErrorDesc().contains(SQL_EXCEPTION_STR)) {
				errorResponse.setErrorDesc(errorResponse.getErrorDesc().replace(SQL_EXCEPTION_STR,EMPTY_STR));
			}
			errorResponse.setExceptionClass(baseException.getClass().getName());
		}

		LOG.debug("handleException is completed");

		return new ResponseEntity<>(errorResponse,
				(errorResponse != null && errorResponse.getStatus() != null) ? errorResponse.getStatus()
						: HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	
	@ExceptionHandler(Exception.class)
	protected ResponseEntity<ErrorDetails> handleGenericException(Exception baseException, WebRequest request) {
		LOG.debug("Starting handleGenericException");
		ErrorDetails errorResponse = null;

		LOG.error("****Generic Exception: ", baseException);

		String errorMessage = org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage(baseException);
		if(errorMessage.contains(SQL_EXCEPTION_STR)) {
			errorMessage= errorMessage.replace(SQL_EXCEPTION_STR,EMPTY_STR);
		}
		errorResponse = new ErrorDetails(ErrorCodes.A001, errorMessage);

		LOG.debug("handleGenericException is completed");

		return new ResponseEntity<>(errorResponse,errorResponse.getStatus());
	}

	/**
	 * Customize the response for MethodArgumentNotValidException.
	 * <p>This method delegates to {@link #handleExceptionInternal}.
	 * @param ex the exception
	 * @param headers the headers to be written to the response
	 * @param status the selected response status
	 * @param request the current request
	 * @return a {@code ResponseEntity} instance
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {

		String message = exception.getMessage();
		if (!CollectionUtils.isEmpty(exception.getBindingResult().getFieldErrors())) {
			List<String> fieldErrors = new ArrayList<>();
			for (FieldError fieldError : exception.getBindingResult().getFieldErrors()) {
				fieldErrors.add(fieldError.getField() + " -> " + fieldError.getDefaultMessage());
			}
			message = fieldErrors.toString();
		} else if (!CollectionUtils.isEmpty(exception.getBindingResult().getAllErrors())) {
			message = exception.getBindingResult().getAllErrors().toString();
		}
		ErrorDetails errorResponse = new ErrorDetails(HttpStatus.BAD_REQUEST.toString(), message);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
	}
	
	

}
