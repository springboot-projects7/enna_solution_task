package com.chandan.ennea.exception;


import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import org.springframework.http.HttpStatus;

public enum ErrorCodes {
	 A001(INTERNAL_SERVER_ERROR, "Generic application error."),
	 SQL004(BAD_REQUEST, "JPA Generic Error");
	
	
	private final String description;
    private final HttpStatus httpStatus;

    private ErrorCodes(HttpStatus httpStatus, String description) {
        this.description = description;
        this.httpStatus = httpStatus;
    }

    public String getDescription() {
        return description;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
