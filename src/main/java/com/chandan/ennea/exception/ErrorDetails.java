package com.chandan.ennea.exception;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ErrorDetails implements Serializable {

	private static final long serialVersionUID = -2002539328792733554L;

	private HttpStatus status;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private LocalDateTime timestamp = LocalDateTime.now();
	private String errorCode;
	private String errorMessage;
	private String errorDesc;
	private String exceptionClass;

	/**
	 * Constructs the ErrorDetails with error code along with message.
	 *
	 * @param errorCode
	 *            the String
	 * @param errorMessage
	 *            the String
	 * @param errorDesc
	 *            the String
	 */
	public ErrorDetails(ErrorCodes code, String errorDesc) {
		this.status = code.getHttpStatus();
		this.timestamp = getTimestamp();
		this.errorCode = code.toString();
		this.errorMessage = code.getDescription();
		this.errorDesc = errorDesc;
	}

	/**
	 * @param code
	 */
	public ErrorDetails(ErrorCodes code) {
		this.status = code.getHttpStatus();
		this.timestamp = getTimestamp();
		this.errorCode = code.toString();
		this.errorMessage = code.getDescription();
	}

	public ErrorDetails(HttpStatus status, String errorCode, String errorMessage) {
		this.status = status;
		this.timestamp = getTimestamp();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	/**
	 * @param errorCode
	 * @param errorMessage
	 */
	public ErrorDetails(final String errorCode, final String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
}
