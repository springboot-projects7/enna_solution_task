package com.chandan.ennea.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.chandan.ennea.entity.Product;


public interface ProductRepository extends JpaRepository<Product, String> {
	
	@Query("select p from Product p where p.exp > :currentDate and p.suppiler in (:suppiler)")
//	List<Product> findByExpGreaterThanCurrentDateAndSuppilerIn(LocalDate currentDate, List<String> suppiler);
	List<Product> findByExpGreaterThanCurrentDateAndSuppilerIn(LocalDate currentDate, List<String> suppiler,  Pageable pageable);
	
	Product findAllByName(String name);
	
	List<Product> findBySuppilerAndStockGreaterThan(String supplier, Integer stock, Pageable pageable);

}
