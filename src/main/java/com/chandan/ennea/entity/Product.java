package com.chandan.ennea.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "product_details")
public class Product {

	@Id
	@Column(name = "code")
	private String code;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "batch")
	private String batch;
	
	@Column(name = "stock")
	private Integer stock;
	
	@Column(name = "deal")
	private Integer deal;
	
	@Column(name = "free")
	private Integer free;
	
	@Column(name = "mrp")
	private Double mrp;
	
	@Column(name = "rate")
	private Double rate;
	
	@Column(name = "exp")
	private LocalDate exp;
	
	@Column(name = "company")
	private String company;
	
	@Column(name = "suppiler")
	private String suppiler;
	
	
}
