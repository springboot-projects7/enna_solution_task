package com.chandan.ennea.constants;

public class MethodNames {

	public static final String GET_DATA_BY_PAGE = "getDataByPage";
	public static final String SAVE_FILE = "saveFile";
	public static final String GET_ALL_PRODUCT_DETAILS = "getAllProducts";
	public static final String GET_PRODUCT_DETAILS_BY_NAME = "getByProductName";
	public static final String GET_BY_SUPPILERS_NAME_WITH_STOCKS = "getBySuppilersNameWithStock";
	public static final String GET_DATA_BY_SUPPILERNAME = "getBySuppilerNameAndStockGreaterThanZero";
	public static final String GET_DATA_BY_PRODUCT_NAME = "getByProductName";
}
