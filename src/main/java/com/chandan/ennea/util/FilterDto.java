package com.chandan.ennea.util;

import java.util.List;

import lombok.Data;

@Data
public class FilterDto {
	List<String> suppilerName;
}
