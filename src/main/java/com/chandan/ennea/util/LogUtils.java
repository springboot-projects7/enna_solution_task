package com.chandan.ennea.util;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component("logUtils")
public class LogUtils {

    /** The logger. */
    private static final Logger LOG = LoggerFactory.getLogger(LogUtils.class);

    private static final String ERROR_LOG_DESC = "Exception :: %s :: %s";
    private static final String EXCEPTION_LOG_DESC = "Exception :: %s :: %s ::";
    private static final String START_DEBUG_LOG_DESC = "START :: %s";
    private static final String INFO_LOG_DESC = "INFO :: %s :: %s";
    private static final String DEBUG_LOG_DESC = "DEBUG :: %s :: %s";
    private static final String END_DEBUG_LOG_DESC = "END :: %s ::";
    private static final String ERROR_LOG_DESC_REQ_RES = "Exception :: %s :: Custom Message -  %s :: Request - %s :: Response - %s :: Error - %s";
    /**
     *
     */
    public static final BiConsumer<String, String> basicErrorLog = (methodInfo, description) -> {
        if (LOG.isErrorEnabled()) {
            LOG.error(String.format(ERROR_LOG_DESC, methodInfo, description));
        }
    };

    public static final BiConsumer<String, Throwable> throwableErrorLog = (methodInfo, throwable) -> {
        if (LOG.isErrorEnabled()) {
            LOG.error(String.format(ERROR_LOG_DESC, methodInfo, org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage(throwable)),throwable);
        }
    };

    public static final BiConsumer<String, Throwable> throwableBasicErrorLog = (methodInfo, throwable) -> {
        if (LOG.isErrorEnabled()) {
            LOG.error(String.format(ERROR_LOG_DESC, methodInfo, org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage(throwable)));
        }
    };

    public static GenericFunctions.TriConsumer<String, String, Exception> errorLog = (methodInfo, description, exception) -> {
        if (LOG.isErrorEnabled()) {
            LOG.error(String.format(EXCEPTION_LOG_DESC, methodInfo, description), org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(exception));
        }
    };

    /**
     *
     */
    public static final Consumer<String> basicDebugLog = methodInfo -> {
        if (LOG.isDebugEnabled()) {
            LOG.debug(methodInfo);
        }
    };



    /**
     *
     */
    public static final Consumer<String> basicInfoLog = LOG::info;

    /**
     *
     */
    public static final Consumer<String> startDebugLog = methodInfo -> {
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format(START_DEBUG_LOG_DESC, methodInfo));
        }
    };

    public static final BiConsumer<String, String> infoLog = (methodInfo, description) -> {
        if (LOG.isInfoEnabled()) {
            LOG.info(String.format(INFO_LOG_DESC, methodInfo, description));
        }
    };

    public static final BiConsumer<String, String> debugLog = (methodInfo, description) -> {
        if (LOG.isDebugEnabled()) {
            LOG.info(String.format(DEBUG_LOG_DESC, methodInfo, description));
        }
    };

    /**
     *
     */
    public static final Consumer<String> endDebugLog = methodInfo -> {
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format(END_DEBUG_LOG_DESC, methodInfo));
        }
    };

    public static final GenericFunctions.PentaConsumer<String, String, String, String, Throwable> throwableErrorLogWithReqRes = (methodInfo, message, request, response, throwable) -> {
        if (LOG.isErrorEnabled()) {
            LOG.error(String.format(ERROR_LOG_DESC_REQ_RES, methodInfo, message, request, response, org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(throwable)));
        }
    };

    public static final GenericFunctions.PentaConsumer<String, String, String, String, Throwable> throwableBasicErrorLogWithReqRes = (methodInfo, message, request, response, throwable) -> {
        if (LOG.isErrorEnabled()) {
            LOG.error(String.format(ERROR_LOG_DESC_REQ_RES, methodInfo, message, request, response, org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage(throwable)));
        }
    };
}

