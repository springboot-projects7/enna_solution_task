package com.chandan.ennea.util;
import java.util.function.BiConsumer;

import com.chandan.ennea.exception.ErrorCodes;
import com.chandan.ennea.exception.ErrorDetails;
import com.chandan.ennea.exception.ProductNotFoundException;

public class ExceptionUtils {

	private static final String ERROR_LOG_DESC = "%s :: %s";


	private static final BiConsumer<String, Exception> logError = (methodInfo, ex) -> {
		if (ex instanceof ProductNotFoundException)
			LogUtils.throwableBasicErrorLog.accept(methodInfo, ex);
		else
			LogUtils.throwableErrorLog.accept(methodInfo, ex);
	};

	/**
	 * 
	 */
	public static GenericFunctions.ThrowableTriConsumer<String, Exception, String, ProductNotFoundException> handleException = (methodInfo, ex, message) -> {
		if (!(ex instanceof ProductNotFoundException)) {
			logError.accept(String.format(ERROR_LOG_DESC, methodInfo, message), ex);
			

			throw new ProductNotFoundException(new ErrorDetails(ErrorCodes.A001,
					org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage(ex)));
		} else {
			logError.accept(String.format(ERROR_LOG_DESC, methodInfo, message), ex);
			throw (ProductNotFoundException) ex;
		}
	};
}
