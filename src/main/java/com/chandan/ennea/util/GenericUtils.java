package com.chandan.ennea.util;

import java.util.function.BiFunction;

import javax.persistence.EntityNotFoundException;
import javax.xml.bind.ValidationException;

import com.chandan.ennea.exception.ErrorCodes;
import com.chandan.ennea.exception.ErrorDetails;
import com.chandan.ennea.exception.ProductNotFoundException;




/**
 * @author bhavya
 *
 */
public class GenericUtils {

	private static final String APPLICATION_NAME = "ProductDetails";


	public static GenericFunctions.TriFunction<String, String, String, String> methodInfo = (moduleName, className, methodName) -> String
			.format("%s:%s:%s:%s", APPLICATION_NAME, moduleName, className, methodName);

	public static BiFunction<String, String, String> basicMethodInfo = (className, methodName) -> String
			.format("%s:%s:%s", APPLICATION_NAME, className, methodName);

	public static GenericFunctions.TetraFunction<String, String, String, String, String> erroredMethodInfo = (moduleName, className, methodName, errorCode) -> String
			.format("%s:%s:%s:%s:%s", APPLICATION_NAME, moduleName, className, methodName, errorCode);

//	public static GenericFunctions.ThrowableBiConsumer<ErrorCodes, String, ProductNotFoundException> throwCommonNullVariableException = (code, description) -> {
//		throw new ValidationException(new ErrorDetails(code, description));
//	};
//
//	public static GenericFunctions.ThrowableConsumer<ErrorCodes, ProductNotFoundException> throwCommonNullVariableExceptionString = (code) -> {
//		throw new ValidationException(new ErrorDetails(code));
//	};

	public static BiFunction<StringBuilder, String, StringBuilder> buildErrorMessage = (builder, variable) -> builder
			.append(String.format("%s can't be null", variable)).append(System.lineSeparator());

//	public static GenericFunctions.ThrowableConsumer<ErrorCodes, ProductNotFoundException> throwCommonEntityNotFoundException = (code) -> {
//		throw new EntityNotFoundException(new ErrorDetails(code));
//	};

	public static String surroundPercents(String value) {
		return "%" + value + "%";
	}
}
