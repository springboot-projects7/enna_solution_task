package com.chandan.ennea.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.chandan.ennea.entity.Product;

public interface ProductService {

	void save(MultipartFile file);

	Map<String, Object> getAllProducts(Pageable pageable);
	
	Product getByProductName(String prodName);
	
	Map<String, Object> getBySuppilerNameAndStockGreaterThanZero(String supplier, Integer stock, Pageable pageable);
	
	Map<String, Object> getBySuppliersWithNotExpaired(LocalDate currentDate, List<String> suppiler, Pageable pageable);
	
	
	

}
